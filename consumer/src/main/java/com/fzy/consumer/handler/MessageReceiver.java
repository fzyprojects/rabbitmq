package com.fzy.consumer.handler;

import com.fzy.consumer.model.ChatMessage;
import com.fzy.consumer.model.OjmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import static com.fzy.provider.constant.RabbitConstants.*;

/**
 * 当Exchange和RoutingKey相同、queue不同时，所有消费者都能消费同样的信息；
 * 当Exchange和RoutingKey、queue都相同时，消费者中只有一个能消费信息，其他消费者都不能消费该信息。
 * <p>
 * 解释: 如果多个消费者的queue不同时，相当于每个消费者根据routingKey从Exchange中获取一个队列，
 * 所以这些消费者的队列是相同的，都能获取队列中的全部内容
 * <p>
 * 如果多个queue相同时，说明多个消费者共用一个队列，因此队列中的信息只能按顺序分发给消费者，
 * 比如一个消费者消费完消息"message1"之后，其他消费者不能再消费消息message1
 */
@Slf4j
@Component
public class MessageReceiver {

    @Resource
    private OjmService ojmService;

    //=============================================Topic交换机Begin=====================================================//

    /**
     * 队列QUEUE_TOPIC在配置文件里已经通过routingKey和EXCHANGE_TOPIC绑定了
     *
     * @param chatMessage
     */
    @RabbitListener(queues = QUEUE_TOPIC)
    public void consumer1(@Payload String chatMessage) {
        ChatMessage message = ojmService.object(chatMessage, ChatMessage.class);
        message.setConsumed(true);
        log.info("consumer1 - {}", message);
    }

    /**
     * 不在配置文件里绑定，则需要在注解里绑定
     *
     * @param chatMessage
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = QUEUE_TOPIC),
            exchange = @Exchange(name = EXCHANGE_TOPIC, type = ExchangeTypes.TOPIC),
            key = ROUTING_KEY
    ))
    public void consumer2(@Payload String chatMessage) {
        ChatMessage message = ojmService.object(chatMessage, ChatMessage.class);
        message.setConsumed(true);
        log.info("consumer2 - {}", message);
    }

    /**
     * 交换机和routingKey相同，队列不同
     *
     * @param chatMessage
     */
    @RabbitListener(queues = QUEUE_TOPIC_1)
    public void consumer3(@Payload String chatMessage) {
        ChatMessage message = ojmService.object(chatMessage, ChatMessage.class);
        message.setConsumed(true);
        log.info("consumer3 - {}", message);
    }

    //=============================================Topic交换机End========================================================//
    //=============================================Fanout交换机Begin=====================================================//

    /**
     * Fanout交换机，群发消息
     *
     * @param chatMessage
     */
    @RabbitListener(queues = QUEUE_FANOUT_1)
    public void consumer4(@Payload String chatMessage) {
        ChatMessage message = ojmService.object(chatMessage, ChatMessage.class);
        message.setConsumed(true);
        log.info("consumer4 - {}", message);
    }

    /**
     * 同上
     *
     * @param chatMessage
     */
    @RabbitListener(queues = QUEUE_FANOUT_2)
    public void consumer5(@Payload String chatMessage) {
        ChatMessage message = ojmService.object(chatMessage, ChatMessage.class);
        message.setConsumed(true);
        log.info("consumer5 - {}", message);
    }
    //=============================================Fanout交换机End=======================================================//
    //=============================================Direct交换机Begin=====================================================//

    @RabbitListener(queues = QUEUE_DIRECT_1)
    public void consumer6(@Payload String chatMessage) {
        ChatMessage message = ojmService.object(chatMessage, ChatMessage.class);
        message.setConsumed(true);
        log.info("consumer6 - {}", message);
    }
}
