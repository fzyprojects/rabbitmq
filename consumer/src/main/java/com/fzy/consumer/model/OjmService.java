package com.fzy.consumer.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;

@Slf4j
@Service
public class OjmService {

    @Resource
    private ObjectMapper objectMapper;

    public String json(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error("json parse error - {}", object);
        }
        return null;
    }

    public <T> T object(String content, Class<T> valueType) {
        try {
            return objectMapper.readValue(content, valueType);
        } catch (IOException e) {
            log.error("[{}] cannot be parsed to [{}]", content, valueType);
        }
        return null;
    }

    public <T> Map parseObjectToMap(T t) {
        String json = this.json(t);
        Map map = this.object(json, Map.class);
        return map;
    }
}
