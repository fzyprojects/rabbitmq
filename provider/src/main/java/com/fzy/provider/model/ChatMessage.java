package com.fzy.provider.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ChatMessage implements Serializable{
    private final static Long serializableID = 1L;
    private String messageID;
    private String messageBody;
    private boolean consumed;
}
