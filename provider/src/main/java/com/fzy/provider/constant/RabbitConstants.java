package com.fzy.provider.constant;

/**
 * @author km005
 * @date 2020-01-20 11:36 上午
 */
public class RabbitConstants {
    public static final String EXCHANGE_DIRECT = "EXCHANGE_DIRECT";
    public static final String EXCHANGE_FANOUT = "EXCHANGE_FANOUT";

    public static final String EXCHANGE_TOPIC = "EXCHANGE_TOPIC";
    public static final String ROUTING_KEY = "ROUTING_KEY";
    public static final String QUEUE_TOPIC = "QUEUE_TOPIC";
    public static final String QUEUE_TOPIC_1 = "QUEUE_TOPIC_1";

    public static final String QUEUE_FANOUT_1 = "QUEUE_FANOUT_1";
    public static final String QUEUE_FANOUT_2 = "QUEUE_FANOUT_2";

    public static final String QUEUE_DIRECT_1 = "QUEUE_DIRECT_1";
    public static final String QUEUE_DIRECT_2 = "QUEUE_DIRECT_2";
}
