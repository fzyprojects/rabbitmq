package com.fzy.provider.controller;

import com.fzy.provider.model.ChatMessage;
import com.fzy.provider.model.OjmService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static com.fzy.provider.constant.RabbitConstants.*;

@RestController
@RequestMapping("/provider")
public class ProviderController {

    private static int count = 1;

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private OjmService ojmService;

    @GetMapping("")
    public String sendMessage() {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setMessageID(String.valueOf(count++));
        chatMessage.setMessageBody("你好，这是一条消息");
        String message = ojmService.json(chatMessage);
        rabbitTemplate.convertAndSend(EXCHANGE_TOPIC, ROUTING_KEY, message);
        return "发送成功";
    }

    @GetMapping("fanout")
    public String sendFanoutMessage() {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setMessageID(String.valueOf(count++));
        chatMessage.setMessageBody("你好，这是一条消息");
        String message = ojmService.json(chatMessage);
        rabbitTemplate.convertAndSend(EXCHANGE_FANOUT, null, message);
        return "发送成功";
    }

    @GetMapping("direct")
    public String sendDirectMessage() {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setMessageID(String.valueOf(count++));
        chatMessage.setMessageBody("你好，这是一条消息");
        String message = ojmService.json(chatMessage);
        rabbitTemplate.convertAndSend(EXCHANGE_DIRECT, "direct.key1", message);
        return "发送成功";
    }
}
