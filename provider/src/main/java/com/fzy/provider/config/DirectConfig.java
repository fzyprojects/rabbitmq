package com.fzy.provider.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.fzy.provider.constant.RabbitConstants.*;

@Configuration
public class DirectConfig {

    @Bean
    Queue queueDirect1() {
        return new Queue(QUEUE_DIRECT_1);
    }

    @Bean
    Queue queueDirect2() {
        return new Queue(QUEUE_DIRECT_2);
    }

    @Bean
    DirectExchange directExchange() {
        return new DirectExchange(EXCHANGE_DIRECT);
    }

    @Bean
    Binding bindingDirect1() {
        return BindingBuilder.bind(queueDirect1()).to(directExchange()).with("direct.key1");
    }

    @Bean
    Binding bindingDirect2() {
        return BindingBuilder.bind(queueDirect2()).to(directExchange()).with("direct.key1");
    }
}
