package com.fzy.provider.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.fzy.provider.constant.RabbitConstants.*;

/**
 * 将多个队列绑定到同一个Fanout交换机上，实现群发消息的效果
 */
@Configuration
public class FanoutConfig {

    @Bean
    public Queue fanoutQueue1() {
        return new Queue(QUEUE_FANOUT_1);
    }

    @Bean
    public Queue fanoutQueue2() {
        return new Queue(QUEUE_FANOUT_2);
    }

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange(EXCHANGE_FANOUT, true, true);
    }

    /**
     * 将队列"QUEUE_FANOUT_1"绑定到交换机"EXCHANGE_FANOUT"上
     *
     * @return
     */
    @Bean
    Binding fanoutBinding1() {
        return BindingBuilder.bind(fanoutQueue1()).to(fanoutExchange());
    }

    /**
     * 将队列"QUEUE_FANOUT_2"绑定到交换机"EXCHANGE_FANOUT"上
     *
     * @return
     */
    @Bean
    Binding fanoutBinding2() {
        return BindingBuilder.bind(fanoutQueue2()).to(fanoutExchange());
    }
}
