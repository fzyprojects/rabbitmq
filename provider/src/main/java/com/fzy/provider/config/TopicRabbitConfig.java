package com.fzy.provider.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.fzy.provider.constant.RabbitConstants.*;

@Configuration
public class TopicRabbitConfig {

    @Bean
    public Queue topicQueue1() {
        return new Queue(QUEUE_TOPIC);
    }

    @Bean
    public Queue topicQueue2() {
        return new Queue(QUEUE_TOPIC_1);
    }

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange(EXCHANGE_TOPIC);
    }

    @Bean
    Binding bindingExchangeMessage(Queue topicQueue1, TopicExchange topicExchange) {
        return BindingBuilder.bind(topicQueue1).to(topicExchange).with(ROUTING_KEY);
    }

    @Bean
    Binding bindingExchangeMessage1() {
        return BindingBuilder.bind(topicQueue2()).to(topicExchange()).with(ROUTING_KEY);
    }
}
